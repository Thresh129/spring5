package aptech.fpt.spring.config;

import aptech.fpt.spring.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@EnableAutoConfiguration
@Configuration
public class SearchConfiguration {

    @Autowired
    private EntityManager bentityManager;

    @Bean
    SearchService searchService() {
        SearchService hibernateSearchService = new SearchService(bentityManager);
        hibernateSearchService.initializeHibernateSearch();
        return hibernateSearchService;
    }
}
